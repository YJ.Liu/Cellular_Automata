import Rhino.Geometry as rg
import System.Drawing.Color as col

def ColoringPixels(list,M):
    dic = {0:col.Gray, 1:col.Red}
    n=M.Faces.Count
    a=M.Faces.GetFaceVertices(0)
    PixelsList=[]
    for i in range (0,n):
        Pix=rg.Mesh()
        PixVer=(M.Faces.GetFaceVertices(i))
        Pix.Vertices.Add(PixVer[1])
        Pix.Vertices.Add(PixVer[2])
        Pix.Vertices.Add(PixVer[3])
        Pix.Vertices.Add(PixVer[4])
        Pix.Faces.AddFace(0,1,2,3)
        Pix.VertexColors.CreateMonotoneMesh(dic[list[i]])
        PixelsList.append(Pix)
    return PixelsList


a=ColoringPixels(LODC,M)