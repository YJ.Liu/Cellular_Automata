#! /usr/bin/env python

# example from
# https://matplotlib.org/examples/mplot3d/subplot3d_demo.html

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np 
from matplotlib import cm

fig = plt.figure(figsize=plt.figaspect(0.5))
ax = fig.add_subplot(121, projection = '3d')

x = np.arange(-6, 6, 0.25)
y = np.arange(-6, 6, 0.25)
x, y = np.meshgrid(x, y)
#r = np.sqrt(x**2 + y**2)
#z = np.sin(r)

r = np.sqrt(np.sin(x) + np.cos(y))
z = np.sin(r)

surf = ax.plot_surface(x, y, z, rstride=1, cstride=1, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

ax = fig.add_subplot(1, 2, 2, projection='3d')
ax.plot_wireframe(x, y, z, rstride=2, cstride=2)

plt.show()