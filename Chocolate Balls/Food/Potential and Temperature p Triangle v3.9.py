# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 14:35:02 2018

@author: SvanS
This script is written to calculate growth potential of crops in regions:
It is done by calculating the estimated temperature and the gaussian temperature.
And the potential and the actual production of crops.


"""

from matplotlib import pyplot as mp
import math
import numpy as np

Tch= 1.005 #climate change
D = 0.1 #distance from equator, is responsible for standard deviation
s_av_T= [20.,8.,16.,22.] #start av temperature of 4 seasons
MaxT, MinT= 30., 10. #maximum Minimal temperature for crop
i=8 # timeframe in years
aT=[] # empty list with temperature p season p year

def cli_ch(s_av_T,Tch, ic):
    mT=[]
    for av_T in s_av_T:
        mT.append(av_T**Tch**float(ic)) #exponential climate change giving mean T
    return mT

def crop(MaxT,MinT,aT): # is potential of a crop by temperature

    tpz=(MaxT+MinT)/2
    t=aT-tpz
    if t<0:
        t=t*-1
    a=1./((MaxT-tpz)**2.72)
    p=-a*(t** 2.72)+1
    return p


def temp_cli(mT,D): #gaussian distribution
    si = D*mT/1.5 #standard diviation
    return np.random.normal(mT, si)

#ax.plot(mT, temp_cli(mT,D))
#mp.show()de

for ic in range(i):
        aT.append(cli_ch(s_av_T,Tch, ic))
print "estimated temperature(winter, spring, summer, autumn):"
print aT     

gT=[] # gaussian temperature
for S in aT:
        li=[]
        for T in S:
            li.append(temp_cli(T,D))            
        gT.append(li)
print "actual temperature(winter, spring, summer, autumn):"
print gT
   
def pot_yi(i,Tch,D,s_av_T, MaxT, MinT): # potentialyield: timeframe, climate change,start av temperature of 4 seasons, max/min t crop)
    Pt=[] # list with estemated potential per season
    for S in aT:
        li=[]
        for T in S:
            v=crop(MaxT,MinT,T)
            if v>0.1:
                li.append(v)
            else:
                li.append(0.)
        Pt.append(li)
    return Pt
potential_yield=pot_yi(i,Tch,D,s_av_T, MaxT, MinT)
print "potential yield(winter, spring, summer, autumn):"
print pot_yi(i,Tch,D,s_av_T, MaxT, MinT)


def act_yi(i,Tch,D,s_av_T, MaxT, MinT):# actual yield
    AY=[] # Actual Yield based on random of gaussian
    for S in gT:
        li2=[]
        for nT in S:
            v=crop(MaxT,MinT,nT)
            if v>0.1:
                li2.append(v)
            else:
                li2.append(0.)
        AY.append(li2)
    return AY
actual_yield=act_yi(i,Tch,D,s_av_T, MaxT, MinT)
print "actual yield(winter, spring, summer, autumn):"
print actual_yield

time = np.arange(0., i , 1)

plot_pot_y=[] # this list is used to plot the potential (not used in this file)
av_pot_yield=[] # this list contains annual potential data
for ii in potential_yield:
    av_pot_yield.append(sum(ii)/len(ii))
    for ir1 in ii:
        plot_pot_y.append(ir1)
print "annualy estimated potential:"
print av_pot_yield

plot_act_y=[] # this list is used to plot the actual (not used in this file)
av_act_yield=[] # this list contains annual actual data
for ii in actual_yield:
    av_act_yield.append(sum(ii)/len(ii))
    for ir2 in ii:
        plot_act_y.append(ir2)
print "annualy actual potential:"
print av_act_yield

mp.plot(time, av_act_yield, 'ro-', time, av_pot_yield, 'go--')
        
    

