# -*- coding: utf-8 -*-
"""
Created on Tue May 29 10:49:05 2018

@author: Hellmer
"""

import pandas as pd
import numpy as np
import random
import matplotlib.pyplot as plt
import time

df_markov=pd.read_csv("Mvch_10_normal.csv") # reads csv with markov probabilites
df_markov.set_index("a", inplace=True)
markov=np.array(df_markov*100) #creates array

economy=pd.read_csv("T20_Economy.csv")



def markovreader(name):
    df = pd.read_csv(name)
    df.set_index("a",inplace = True)
    markoviso = np.array(df*100)
    return markoviso

#################### MARKOV DATABASE ###################################
    

df_markoviso=pd.read_csv("Mvch_10_iso.csv") # reads csv with markov probabilites
df_markoviso.set_index("a", inplace=True)
markoviso=np.array(df_markoviso*100) #creates array

m_ai_gdp = markovreader("M_ai_gdp.csv")
m_ai_trade = markovreader("M_ai_trade.csv")
m_ai_unemp = markovreader("M_ai_unemp.csv")
m_ai_resources = markovreader("M_ai_resources.csv")
m_ai_io = markovreader("M_ai_io.csv")
m_ai_epi = markovreader("M_ai_epi.csv")

m_in_up = markovreader("M_ai_gdp.csv")
m_in_down = markovreader("M_ai_gdp.csv")

m_iso_gdp = markovreader("M_iso_gdp.csv")
m_iso_trade = markovreader("M_iso_trade.csv")
m_iso_unemp = markovreader("M_iso_unemp.csv")
m_iso_resources = markovreader("M_iso_resources.csv")
m_iso_io = markovreader("M_iso_io.csv")
m_iso_epi = markovreader("M_iso_epi.csv")
markovineq = markovreader("M_mayhem.csv")
markovmoney = markovreader("M_moneyrains.csv")

markovai=m_ai_io


#################### MARKOV DATABASE ###################################

def df_discretiser_10(df, dfcomp): # discretises dataframe into highs and lows depending on their average
    df2=pd.DataFrame() 
    def col_average (datafrm, col_name): #creates average from a column
        col_value=datafrm[[col_name]]
        nums=col_value.loc[1:len(col_value)]
        y=np.mean(nums)
        x=float(y)
        return x  
    collist=list(df.columns)
    for i in collist:
        bins = [dfcomp[i].min(),0.1*dfcomp[i].max(),0.2*dfcomp[i].max(),0.3*dfcomp[i].max(),0.4*dfcomp[i].max(),0.5*dfcomp[i].max(),0.6*dfcomp[i].max(),0.7*dfcomp[i].max(),0.8*dfcomp[i].max(),0.9*dfcomp[i].max(),dfcomp[i].max()]
        names = [0,1,2,3,4,5,6,7,8,9]
        df2[i]=pd.cut(df[i], bins, labels = names)
    df2.fillna(0, inplace=True)
    return df2

def logistic (func_range, midpoint, steepness, domain): #function for smooth transition between numbers
    return func_range / (1+np.exp(-1 * steepness * (domain-midpoint)))

econ_d=df_discretiser_10(economy, economy)
econ_t=pd.DataFrame()
econ_t[0]=econ_d["EPI"]


def myRand(i, w):
    r = random.uniform(0, sum(w))
    # loop through a list of inputs and max cutoff values, returning
    # the first value for which the random num r is less than the cutoff value
    for n,v in map(None, i,[sum(w[:x+1]) for x in range(len(w))]):
        if r < v:
            return n

def finite_state_10 (value, markov): # finite state machine with ten states
    v = value
    m = markov
    s = [0,1,2,3,4,5,6,7,8,9]
    if v in s:
        m = list(markov[v])
        r = myRand(s,m)
        return (((r+v)/2)+v)/2
        


###################################################################


def simulation_io(length, dday): #simulates the worlds economy under isolationism
    x = 1
    years = length #length of simulation
    yearsx = dday #year isolationism enters
    
    while x <= yearsx: #creates dataframe for 100 years
        l0=list(econ_t[x-1])
        l1=[]
        for i in l0:
            a = finite_state_10(i, markov)
            l1.append(a)
        
        econ_t[x]=pd.DataFrame(l1)
        x += 1
        
    while yearsx < x <= years:  #creates dataframe for 100 years
        l0=list(econ_t[x-1])
        l1=[]
        for i in l0:
            a = finite_state_10(i, markoviso)
            l1.append(a)
        
        econ_t[x]=pd.DataFrame(l1)
        x += 1
        
    x=range(1,years) # years to be plotted on graph
    y=[] # sum of all values in a year ( planet score?)
    
    for i in range (1,years): #sum of all values ---> 0 < SCORE < 200
        lst = list(econ_t[i])
        s = sum(lst)
        y.append(s)
        
    plt.xlim((0,100))   
    plt.plot(x, y)
    plt.show()


def finstat(policy1,policy2,policy3,policy4,factor): #plots simulation under different conditions
    x = 1
    turnlenght = 20
    econ = pd.DataFrame()
    econ[0]=econ_d[factor]
    factor = str(factor)

    while x <= turnlenght*1:      #creates dataframe for 100 years
        l0=list(econ[x-1])
        l1=[]
        for i in l0:
            a = finite_state_10(i, policy1)
            l1.append(a)
        
        econ[x]=pd.DataFrame(l1)
        x += 1
    while x <= turnlenght*2:      #creates dataframe for 100 years
        l0=list(econ[x-1])
        l1=[]
        for i in l0:
            a = finite_state_10(i, policy2)
            l1.append(a)
        
        econ[x]=pd.DataFrame(l1)
        x += 1
    while x <= turnlenght*3:      #creates dataframe for 100 years
        l0=list(econ[x-1])
        l1=[]
        for i in l0:
            a = finite_state_10(i, policy3)
            l1.append(a)
        
        econ[x]=pd.DataFrame(l1)
        x += 1
    while x <= turnlenght*4:      #creates dataframe for 100 years
        l0=list(econ[x-1])
        l1=[]
        for i in l0:
            a = finite_state_10(i, policy3)
            l1.append(a)
        
        econ[x]=pd.DataFrame(l1)
        x += 1
        
    x=range(1,turnlenght*4) # years to be plotted on graph
    y=[] # sum of all values in a year ( planet score?)
    
    for i in range (1,turnlenght*4): #sum of all values ---> 0 < SCORE < 200
        lst = list(econ[i])
        s = sum(lst)
        y.append(s)
    
    y2 = []
    for i in range(1,80):
        aa = 10
        y2.append(aa)

    plt.xlim((0,80))
    plt.ylim((0,100))
    
    plt.plot(x, y)
    plt.legend([factor])



def decider(decision):
    if decision == isolation:
        return markoviso
    if decision == AI:
        return markovai
    if decision == none:
        return markov
    if decision == inequality:
        return markovineq
    if decision == dollarrain:
        return markovmoney
 

df = pd.read_csv("CA_tencountries.csv")
df["GDPcap"] = df["GDP"]/df["Population"]*1000000
pr = pd.read_csv("CA_ten_proximity.csv")
pr.set_index("triangle", inplace = True)
pro = np.array(pr) #proximity list



AI = "AI"
isolation = "isolation"
none = "AI"
inequality = "inequality"
dollarrain = "dollarrain"



############ LITTLE GAME ##################################

print "\n"
print ("Welcome to the Planet Checker")
time.sleep(1)
print ("\n")
name = raw_input ("Please enter your name:  ")
print ("\n")
print 'Hello Chairman Kim Jong '+name,'! You will now rule the world under specific policies: inequality, isolation, AI or none.'

time.sleep(2)
pol1 = raw_input("Please enter the policy for the first 20 years: ")
time.sleep(1)
pol2 = raw_input("Now enter the policy for the next 20 years: ")
time.sleep(1)
pol3 = raw_input("Now enter the policy for the next 20 years: ")
time.sleep(1)
pol4 = raw_input("Now enter the policy for the next 20 years: ")
time.sleep(1)

p1 = decider(pol1)
p2 = decider(pol2)
p3 = decider(pol3)
p4 = decider(pol4)
print "\n"
print ("These are the global economic attributes after your regime. Hope you are happy, little dictator.")
time.sleep(2)

finstat(p1,p2,p3,p4,"GDP")
finstat(p1,p2,p3,p4,"Unemp")
finstat(p1,p2,p3,p4,"Trade")
finstat(p1,p2,p3,p4,"Resources")
finstat(p1,p2,p3,p4,"IO")

plt.legend(["GDP", "Unemp", "Trade", "Resources", "IO"])
plt.show()

time.sleep(2)
print (" *drops mic* ")
    
