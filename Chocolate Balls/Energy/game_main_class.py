"""
BZZZ ZZZy  ZZZZZZZ  ,ZZZZZZZ  ZZZB ZZZ    ZZZZZz    ZZZ,ZZZj  ZZZZ ZZZ       ZZZ BZZZ  DZZZ,  ZZZZ  ZZZZ
 ZZ   Z5    Zj w 5   wZ   j,   zZ  ZW    ZZ   yZE   DZw  Z9    Z Zw ZW       Zy ZZ     9Z     Z,Zj Z Z
 ZZZZZZz    ZZZZ     jZZZz      8ZZD     ZZ    ZZ   zZj  ZZ    Z 8Z Zy       ZZZZ      ZZ     Z EZ Z Z
 ZZ   ZD    Zy  DZ   WZ   ZB     ZZ      ZZj  ZZD   8ZD  ZZ    Z  Z8Z5       ZD 8Zy    EZ     Z  ZZj Z
8ZZZ ZZZD  ZZZZZZZ  WZZZZZZ9    ZZZZ      jZZZZ      8ZZZZ    ZZZ wZZz       ZZZy9ZZZ  zZZZw  ZZZ ZZ ZZZ

20180627 @Heeyoun Kim
2018 Why Factory_Future Model_Team Energy

##############################################################
### whole execution codes are caged as class for HOUDINI   ###
##############################################################
# If you test this code, you should prepare two dummy triangle data. with execution codes.
## This class will work as overal controler in the game.
#turn start
#return_datas to the internal database
"""

from distribution_Controller import distribution_Controller as dc
from calc_Engine import calc_energygame as ce
print('NOTICE : import modules')

engine = ce()
dist = dc()

class main_game:

    ####################################
    ### I N I T I A L I Z A T I O N  ###
    ####################################

    def __init__(self):
        self.triangle_a = {'land': 0.0, 'mountain': 0.0, 'urban': 0.0, 'population': 0.0, 'population_default': 0.0,
                  'temperature': 0.0, 'temp_delta': 0.0,
                  'energy_demands': 0.0, 'pollution': 0.0, 'energy_supply': 0.0, 'transition_value': 0.0,
                  'oil_reserves': 0.0, 'oil_production': 0.0,
                  'coal_reserves': 0.0, 'coal_production': 0.0,
                  'gas_reserves': 0.0, 'gas_production': 0.0,
                  'solar_potential': 0.0, 'rain_potential': 0.0, 'wind_potential': 0.0, 'water_potential': 0.0,
                  'solar_production': 0.0, 'rain_production': 0.0, 'wind_production': 0.0, 'water_production': 0.0,
                  'total_potential': 0.0
                  }

        self.triangle_b = {'land': 0.0, 'mountain': 0.0, 'urban': 0.0, 'population': 0.0, 'population_default': 0.0,
                  'temperature': 0.0, 'temp_delta': 0.0,
                  'energy_demands': 0.0, 'pollution': 0.0, 'energy_supply': 0.0, 'transition_value': 0.0,
                  'oil_reserves': 0.0, 'oil_production': 0.0,
                  'coal_reserves': 0.0, 'coal_production': 0.0,
                  'gas_reserves': 0.0, 'gas_production': 0.0,
                  'solar_potential': 0.0, 'rain_potential': 0.0, 'wind_potential': 0.0, 'water_potential': 0.0,
                  'solar_production': 0.0, 'rain_production': 0.0, 'wind_production': 0.0, 'water_production': 0.0,
                  'total_potential': 0.0
                  }

        self.distance = 0
        self.oil_prod = 0
        self.coal_prod = 0
        self.gas_prod = 0
        self.dist_pattern = 0
        self.turn_no = 0

    #############################
    ###  S T A R T   T U R N  ###
    #############################

    def start_turn(self, value1, value2, value3, value4, value5, value6, value7, value8):

        #########################
        ### A R G U M E N T S ###
        #########################
        """
        self.triangle_a = value1
        self.triangle_b = value2
        self.distance = value3
        self.oil_prod = value4
        self.coal_prod = value5
        self.gas_prod = value6
        self.dist_pattern = value7
        self.turn_no = value8
        """

        print("NOTICE : game is starting")

        #################################
        ### S E T T I N G    D A T A  ###  !- actually, this part can be altered with self variables.
        #################################
        triangle_a = value1
        triangle_b = value2
        oil_production = value3
        coal_production = value4
        gas_production = value5
        dist_pattern = value6
        dist_distance= value7
        turn_in = value8

        ##########################
        ### B A S E  V A L U E ###
        ##########################

        population_a = triangle_a['population']
        population_b = triangle_b['population']

        population_a = engine.population_calc(population_a, turn_in, triangle_a['pollution'])
        population_b = engine.population_calc(population_b, turn_in, triangle_b['pollution'])


        temperature_a = triangle_a['temperature']
        temperature_b = triangle_b['temperature']

        energy_capita_a = engine.en_calc(temperature_a)
        energy_capita_b = engine.en_calc(temperature_b)

        energy_demand_a = engine.ed_calc(energy_capita_a, population_a)
        triangle_a['energy_demand'] = energy_demand_a
        energy_demand_b = engine.ed_calc(energy_capita_b, population_b)
        triangle_b['energy_demand'] = energy_demand_b

        renewable_demand_a = engine.rd_calc(energy_demand_a, triangle_a['transition_value'])
        renewable_demand_b = engine.rd_calc(energy_demand_b, triangle_b['transition_value'])

        #######################################################
        ### F   O   S   S   I   L   E     F   U   E   L   S ###
        #######################################################

        ###################################
        ### F O S S I L   D E M A N D S ###
        ###################################
        fossil_demand_a = engine.fd_calc(energy_demand_a, renewable_demand_a)
        fossil_demand_b = engine.fd_calc(energy_demand_b, renewable_demand_b)

        ### O I L : A
        oil_prod_a = engine.oil_prod(triangle_a['oil_reserves'], fossil_demand_a, oil_production)
        oil_reserve_a = engine.oil_reserve(triangle_a['oil_reserves'], oil_prod_a)
        ### U P D A T E ###
        triangle_a['oil_production'] = oil_prod_a
        triangle_a['oil_reserve'] = oil_reserve_a


        ### C O A L : A
        coal_prod_a = engine.coal_prod(triangle_a['coal_reserves'], fossil_demand_a, coal_production)
        coal_reserve_a = engine.coal_reserve(triangle_a['coal_reserves'], coal_prod_a)
        ### U P D A T E ###
        triangle_a['coal_production'] = coal_prod_a
        triangle_a['coal_reserve'] = coal_reserve_a

        ### G A S : A
        gas_prod_a = engine.gas_prod(triangle_a['gas_reserves'], fossil_demand_a, gas_production)
        gas_reserve_a = engine.gas_reserve(triangle_a['gas_reserves'], gas_prod_a)
        ### U P D A T E ###
        triangle_a['gas_production'] = gas_prod_a
        triangle_a['gas_reserve'] = gas_reserve_a

        fossil_prod_a= engine.fossil_prod(oil_prod_a, coal_prod_a, gas_prod_a)

        ### FOSSIL B ###

        ### O I L : B
        oil_prod_b = engine.oil_prod(triangle_b['oil_reserves'], fossil_demand_b, 0)
        oil_reserve_b = engine.oil_reserve(triangle_b['oil_reserves'], oil_prod_b)
        ### U P D A T E ###
        triangle_b['oil_production'] = oil_prod_b
        triangle_b['oil_reserve_b'] = oil_reserve_b

        ### C O A L : B
        coal_prod_b = engine.coal_prod(triangle_b['oil_reserves'], fossil_demand_b, 0)
        coal_reserve_b = engine.coal_reserve(triangle_b['coal_reserves'], coal_prod_b)
        ### U P D A T E ###
        triangle_b['coal_production'] = coal_prod_b
        triangle_b['coal_reserve_b'] = coal_reserve_b

        ### G A S : B
        gas_prod_b= engine.gas_prod(triangle_b['gas_reserves'], fossil_demand_b, 0)
        gas_reserve_b = engine.gas_reserve(triangle_b['gas_reserves'], gas_prod_b)
        ### U P D A T E ###
        triangle_b['gas_production'] = gas_prod_b
        triangle_b['gas_reserve_b'] = gas_reserve_b

        fossil_prod_b = engine.fossil_prod(oil_prod_b, coal_prod_b, gas_prod_b)

        #########################
        ### R E N E W A B L E ###
        #########################

        ### POTENTIAL
        renew_solar_a = triangle_a['solar_potential']
        renew_rain_a = triangle_a['rain_potential']
        renew_wind_a = triangle_a['wind_potential']
        renew_water_a = triangle_a['water_potential']
        renew_total_a = triangle_a['total_potential']

        renew_solar_b = triangle_b['solar_potential']
        renew_rain_b = triangle_b['rain_potential']
        renew_wind_b = triangle_b['wind_potential']
        renew_water_b = triangle_b['water_potential']
        renew_total_b = triangle_b['total_potential']

        ### RENEWABLE PRODUCTION : A
        solar_prod_a = engine.renew_solar_prod(renew_solar_a, renew_total_a, renewable_demand_a)
        rain_prod_a = engine.renew_rain_prod(renew_rain_a, renew_total_a, renewable_demand_a)
        wind_prod_a = engine.renew_wind_prod(renew_wind_a, renew_total_a, renewable_demand_a)
        water_prod_a = engine.renew_water_prod(renew_water_a, renew_total_a, renewable_demand_a)
        renew_total_prod_a = engine.renew_total_prod(solar_prod_a, rain_prod_a, wind_prod_a, water_prod_a)

        ### U P D A T E ###
        triangle_a['solar_production'] = solar_prod_a
        triangle_a['rain_production'] = rain_prod_a
        triangle_a['wind_production'] = wind_prod_a
        triangle_a['water_production'] = water_prod_a

        ### RENEWABLE PRODUCTION : B
        solar_prod_b = engine.renew_solar_prod(renew_solar_b, renew_total_b, renewable_demand_b)
        rain_prod_b = engine.renew_rain_prod(renew_rain_b, renew_total_b, renewable_demand_b)
        wind_prod_b = engine.renew_wind_prod(renew_wind_b, renew_total_b, renewable_demand_b)
        water_prod_b = engine.renew_water_prod(renew_water_b, renew_total_b, renewable_demand_b)
        renew_total_prod_b = engine.renew_total_prod(solar_prod_b, rain_prod_b, wind_prod_b, water_prod_b)

        ### U P D A T E ###
        triangle_b['solar_production'] = solar_prod_b
        triangle_b['rain_production'] = rain_prod_b
        triangle_b['wind_production'] = wind_prod_b
        triangle_b['water_production'] = water_prod_b

        #####################
        ### O V E R A L L ###
        #####################

        overall_prod_a = engine.overall_production(fossil_prod_a, renew_total_prod_a)
        overall_prod_b = engine.overall_production(fossil_prod_b, renew_total_prod_b)

        #################
        ### D E L T A ###
        #################

        delta_a = engine.delta_calc(energy_demand_a, overall_prod_a)
        delta_b = engine.delta_calc(energy_demand_b, overall_prod_b)
        delta_check = engine.delta_checker(delta_a, delta_b)

        ###############################
        ### D I S T R I B U T I O N ###
        ###############################
        distribution = dist.dist_checker(dist_pattern, dist_distance, delta_check)

        #################################
        ### E N E R G Y   S U P P L Y ###
        #################################
        energy_supply_a = engine.energy_supply(distribution, overall_prod_a, energy_demand_a, delta_a, delta_check)
        triangle_a['energy_supply'] = energy_supply_a   #!----------------------UPDATE
        energy_supply_b = engine.energy_supply(distribution, overall_prod_b, energy_demand_b, delta_b, delta_check)
        triangle_b['energy_supply'] = energy_supply_b   #!----------------------UPDATE

        #########################
        ### P O L L U T I O N ###
        #########################
        pollution_a = engine.pol_calc(fossil_prod_a)
        triangle_a['pollution'] = pollution_a           #!----------------------UPDATE
        pollution_b = engine.pol_calc(fossil_prod_b)
        triangle_b['pollution'] = pollution_b           #!---------------------UPDATE

        #############################
        ### T E M P E R A T U R E ###
        #############################
        temp_change_a = engine.temp_change(pollution_a)
        temp_change_b = engine.temp_change(pollution_b)

        temp_delta_a = engine.temp_delta(triangle_a['temp_delta'], temp_change_a)
        triangle_a['temp_delta'] = temp_delta_a
        temp_delta_b = engine.temp_delta(triangle_b['temp_delta'], temp_change_b)
        triangle_b['temp_delta'] = temp_delta_b

        temp_a = engine.temperature(triangle_a['temperature'], temp_change_a)
        triangle_a['temperature'] = temp_a
        temp_b = engine.temperature(triangle_b['temperature'], temp_change_b)
        triangle_b['temperature'] = temp_b

        ###################################################
        ### P S Y C H O L O G I C A L   D I S T A N C E ###
        ###################################################
        psy_distance_a = engine.psy_distance(temp_delta_a)
        psy_distance_b = engine.psy_distance(temp_delta_b)


        ###########################
        ### E M I G R A T I O N ###
        ###########################

        pop_emig = engine.population_emigrating(energy_supply_a, energy_supply_b, pollution_a, population_a)

        popu_change_a = engine.population_change(population_a, energy_supply_a, energy_supply_b,population_a,pop_emig)
        triangle_a['population'] = popu_change_a         #!----------------------UPDATE
        popu_change_b = engine.population_change(population_b, energy_supply_a, energy_supply_b,population_b,pop_emig)
        triangle_b['population'] = popu_change_b         #!----------------------UPDATE

        #################################
        ###  C I T Y   V A C A N C Y  ###
        #################################

        city_empty_a = engine.city_empty(popu_change_a, pop_emig, triangle_a['population_default'])
        city_empty_b = engine.city_empty(popu_change_b, pop_emig, triangle_b['population_default'])

        ###############################
        ### T R A N S   V A L U E #####
        ###############################

        transition_a = engine.trans_value(triangle_a['transition_value'], psy_distance_a, city_empty_a)
        triangle_a['transition_value'] = transition_a        #!----------------------UPDATE
        transition_b = engine.trans_value(triangle_b['transition_value'], psy_distance_b, city_empty_b)
        triangle_b['transition_value'] = transition_b        #!----------------------UPDATE


        return (triangle_a, triangle_b, pop_emig)

