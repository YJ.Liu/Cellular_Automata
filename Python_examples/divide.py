#! /usr/bin/env python

"""
    The "try:" and "except:" are used to catch bugs that might occur in a
    program without crashing the program. In the exqample below, if there
    was no try: and except:, the program would have crashed if the user
    would input 0 at the second command prompt. With try and except, the user
    gets a warning and the program terminates "gracefully".

    Check the follow webpage to see what exceptions there are
    https://docs.python.org/2/library/exceptions.html
"""

a = input("Give a number: ")
b = input("And another: ")

try:
    print (a / b)
except ZeroDivisionError as e:
    print "You tried to divide by zero!"
    print "Error: %s\n" % e
